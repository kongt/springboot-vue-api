/*
Navicat MySQL Data Transfer

Source Server         : mysql57
Source Server Version : 50718
Source Host           : localhost:3306
Source Database       : vue

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2017-10-19 18:18:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `t_kv`
-- ----------------------------
DROP TABLE IF EXISTS `t_kv`;
CREATE TABLE `t_kv` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tkey` varchar(500) NOT NULL,
  `txt` varchar(5000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_kv
-- ----------------------------
INSERT INTO `t_kv` VALUES ('1', 'name', '乔峰');
