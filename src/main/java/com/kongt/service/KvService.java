package com.kongt.service;

import com.kongt.domain.Kv;
import com.kongt.model.KvSearchModel;
import com.kongt.model.PageModel;

import javax.xml.bind.ValidationException;

public interface KvService {

    Kv getOne(Long id);

    PageModel<Kv> list(KvSearchModel search);

    Kv save(Kv entity) throws ValidationException;

    void deleteById(Long id);

}
