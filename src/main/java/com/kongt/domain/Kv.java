package com.kongt.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**model
 * @author kongt 2017年10月23日
 *
 */
@Entity
@Table(name = "t_kv")
public class Kv{

	public Kv(Long id, String key, String value) { 
		this.id = id;
		this.key = key;
		this.value = value;
	}

	public Kv() { 
	}

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Long id;

	@Column(nullable = false, unique = true,name="tkey")
	private String key;

	@Column(nullable = false, unique = true,name="txt")
	private String value;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
