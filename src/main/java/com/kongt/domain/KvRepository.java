package com.kongt.domain;

import org.springframework.data.jpa.repository.JpaRepository;

/**jpa interface
 * @author kongt 2017年10月23日
 *
 */
public interface KvRepository  extends JpaRepository<Kv, Long>  {
}
