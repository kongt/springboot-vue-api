package com.kongt;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import com.jfinal.template.Engine;
import com.jfinal.template.ext.spring.JFinalViewResolver;
import com.jfinal.template.source.ClassPathSourceFactory;
import org.springframework.context.annotation.EnableAspectJAutoProxy;


/**
 * 应用入口
 *
 * @author kongt 2017年10月23日
 */
@EnableAspectJAutoProxy
@SpringBootApplication
public class Aplication {
    //从配置中读取app名称
    @Value("${app.Name}")
    private String appName;

    //从配置中注入appdesc
    @Value("${app.Desc}")
    private String appDesc;

    //版权
    @Value("${app.Right}")
    private String appRight;


    /**
     * 配置Jfinal视图解析器
     *
     * @return
     */
    @Bean(name = "jFinalViewResolver")
    public JFinalViewResolver getJFinalViewResolver() {
        JFinalViewResolver jfr = new JFinalViewResolver();
        jfr.setDevMode(true);
        //由于spring项目最终是模板放置在jar包中，所以这里设置模板的根目录为.class文件目录
        jfr.setSourceFactory(new ClassPathSourceFactory());
        //模板文件前缀 ，是根目录中的template/view目录中
        jfr.setPrefix("/templates/view/");
        jfr.setSuffix(".html");
        jfr.setContentType("text/html;charset=UTF-8");
        jfr.setOrder(0);
        Engine engine = jfr.getEngine();
        engine.addSharedObject("appName", this.appName);
        engine.addSharedObject("appDesc", this.appDesc);
        engine.addSharedObject("appRight", this.appRight);
        //这里是要先指明的模板公共方法，有两个就调用2次 也可以在_layout.html文件中提前都定义好
        jfr.addSharedFunction("/templates/view/_layout.html");
        return jfr;
    }


    public static void main(String[] args) {
        SpringApplication.run(Aplication.class, args);
    }
}
