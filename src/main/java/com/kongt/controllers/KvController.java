package com.kongt.controllers;

import com.kongt.service.KvService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers;
import org.springframework.data.domain.ExampleMatcher.StringMatcher;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.kongt.domain.Kv;
import com.kongt.domain.KvRepository;
import com.kongt.model.Jstat;
import com.kongt.model.KvPageable;
import com.kongt.model.KvSearchModel;
import com.kongt.model.PageModel;

import javax.xml.bind.ValidationException;

/**
 * resful api
 *
 * @author kongt 2017年10月23日
 */
@RestController
@RequestMapping({"/kv"})
public class KvController {


    @Autowired
    private KvService kvService;


    @RequestMapping({"/insert", "/update"})
    public Jstat<Object> insert(Kv entity) {
        try {
            kvService.save(entity);
        } catch (ValidationException e) {
            return new Jstat<Object>(false, e.getMessage(),null);
        }
        return new Jstat<Object>();
    }


    @RequestMapping({"/delete", "/remove"})
    public Jstat<Object> delete(Long id) {
        kvService.deleteById(id);
        return new Jstat<Object>();
    }


    @RequestMapping({"/search", "/list", "/select"})
    public Jstat<Object> list(KvSearchModel search) {
        return new Jstat<Object>(true, Jstat.SUCCEED_MSG, kvService.list(search));
    }


    @RequestMapping({"/find"})
    public Jstat<Object> find(Long id) {
        Kv entity = kvService.getOne(id);
        return new Jstat<Object>(true, Jstat.SUCCEED_MSG, entity);
    }

}
