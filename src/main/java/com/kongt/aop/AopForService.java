package com.kongt.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class AopForService {


    @Before("execution(* com.kongt.service.KvService.*(..))")
    public void before(JoinPoint joinPoint) {
        System.out.println("xxxx：" + joinPoint.getSignature().getName());
    }


}
