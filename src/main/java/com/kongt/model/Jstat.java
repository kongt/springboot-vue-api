package com.kongt.model;

/**resful返回状态
 * @author Administrator
 * @param <T>
 */
public class Jstat<T extends Object> {

	public final static String SUCCEED_MSG="恭喜您，成功！";
	private boolean succeed=true;
	private String msg=SUCCEED_MSG;
	private T model;
	public boolean isSucceed() {
		return succeed;
	}
	public void setSucceed(boolean succeed) {
		this.succeed = succeed;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public T getModel() {
		return model;
	}
	public void setModel(T model) {
		this.model = model;
	}


	public Jstat(boolean succeed, String msg, T model) {
		this.succeed = succeed;
		this.msg = msg;
		this.model = model;
	}
	public Jstat() {  
	}
	
	
}
