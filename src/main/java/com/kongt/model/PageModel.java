package com.kongt.model;

import java.util.List;
 
/**搜索响应模型
 * @author kongt 2017年10月23日 
 * @param <T>
 */
public class PageModel<T> {

	private long count=0;
	private List<T> rows=null;
	public long getCount() {
		return count;
	}
	public void setCount(long count) {
		this.count = count;
	}
	public List<T> getRows() {
		return rows;
	}
	public void setRows(List<T> rows) {
		this.rows = rows;
	}
	public PageModel(long count, List<T> rows) { 
		this.count = count;
		this.rows = rows;
	}
	public PageModel() { 
	}
	
	
}
