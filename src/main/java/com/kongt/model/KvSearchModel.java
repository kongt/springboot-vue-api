package com.kongt.model;

 
/**kv 参数帮助类
 * @author kongt 2017年10月23日
 *
 */
public class KvSearchModel extends SearchModel { 
	private String keyword;

	public String getKeyword() {
		if(keyword!=null&&keyword.trim().equals("")){
			return null;
		}
		return this.keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
}
