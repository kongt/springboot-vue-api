package com.kongt.model;

/**搜索模型
 * @author kongt 2017年10月23日
 *
 */
public class SearchModel {

	  private int pageIndex=1;
	  private int pageSize=15;
  
	 

 
	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	private String sortFiled=null;
	
	public String getSortFiled() {
		return sortFiled;
	}

	public void setSortFiled(String sortFiled) {
		this.sortFiled = sortFiled;
	}

	private boolean isAsc=true;

	public boolean isAsc() {
		return isAsc;
	}

	public void setAsc(boolean isAsc) {
		this.isAsc = isAsc;
	}
	
	

	 

	 
	
	
	
}
