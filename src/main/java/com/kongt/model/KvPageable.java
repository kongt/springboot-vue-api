package com.kongt.model;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

/**
 * jpa 分页模型
 *
 * @param <T>
 * @author kongt 2017年10月23日
 */
public class KvPageable<T extends SearchModel> implements Pageable {

    private T search;

    public KvPageable() {
    }

    public KvPageable(T search) {
        this.search = search;
    }

    public T getSearch() {
        return search;
    }

    public void setSearch(T search) {
        this.search = search;
    }

    @Override
    public int getPageNumber() {
        return this.search.getPageIndex();
    }

    @Override
    public int getPageSize() {
        return this.search.getPageSize();
    }

    @Override
    public long getOffset() {
        return this.search.getPageSize() * (this.search.getPageIndex() - 1);
    }

    @Override
    public Sort getSort() {
        return Sort.by(this.search.isAsc() ? Direction.ASC : Direction.DESC, this.search.getSortFiled());
    }

    @Override
    public Pageable next() {
        return null;
    }

    @Override
    public Pageable previousOrFirst() {
        return null;
    }

    @Override
    public Pageable first() {
        return null;
    }

    @Override
    public boolean hasPrevious() {
        return false;
    }

}
